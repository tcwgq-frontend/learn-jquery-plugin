package com.tcwgq.mapper;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tcwgq.po.PersonPlus;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:50:17
 * @email tcwgq@outlook.com
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class PersonPlusMapperTest {
	@Autowired
	private PersonPlusMapper mapper;

	@Test
	public void testInsert() {
		PersonPlus personPlus = new PersonPlus();
		personPlus.setName("zhangSan");
		int rows = mapper.insert(personPlus);
		System.out.println(rows);
	}

	@Test
	public void testGet() {
		PersonPlus personPlus = mapper.get(1);
		System.out.println(personPlus);
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

}
