package com.tcwgq.po;

import java.util.ArrayList;
import java.util.List;

public class GridModel<T> {
	private Integer page;
	private Integer total;
	private Integer records;
	private List<T> dataList = new ArrayList<>();

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getRecords() {
		return records;
	}

	public void setRecords(Integer records) {
		this.records = records;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

}
