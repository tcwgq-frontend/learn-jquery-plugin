package com.tcwgq.po;

import java.io.Serializable;
import java.util.Date;

/**
 * @author tcwgq
 * @time 2017年10月25日 下午10:07:02
 * @email tcwgq@outlook.com
 */
public class Person implements Serializable {
	private Integer id;
	private String name;
	private Integer age;
	private Date timeCreated;
	private Date timeModified;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + "]";
	}

}
