<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- ${pageContext.request.contextPath } 这种方式直接取不出来值，怎么回事？-->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<h1>Deferred对象的使用</h1>
</body>
<script type="text/javascript">
	//Deferred的三种状态
	//未完成，继续等待，或者调用progress（）方法指定的回调函数
	//已完成，立即调用done中的回调函数
	//已失败，立即调用fail中的回调函数
	var dtd = $.Deferred();//新建一个Deferred对象
	var wait1 = function(dtd) {
		var tasks1 = function() {
			alert('执行完毕');
			//dtd.resolve();//改变deferred对象的执行状态，由未完成变为已完成，从而调用done中的回调方法
			dtd.reject();//改变deferred对象的执行状态，由未完成变为已失败，从而调用fail中的回调方法
		};
		setTimeout(tasks1, 5000);
		return dtd.promise();//它的作用是，在原来的deferred对象上返回另一个deferred对象，后者只开放与改变执行状态无关的方法（比如done()方法和fail()方法），屏蔽与改变执行状态有关的方法（比如resolve()方法和reject()方法），从而使得执行状态不能被改变。
	};
	var d = wait1(dtd);
	$.when(d).done(function() {
		alert('wait1  执行完毕啦');
	}).fail(function() {
		alert('wait1  执行失败了');
	});
	//dtd.resolve();//马上执行，出现wait1执行完毕，然后出现执行完毕
	d.resolve(); // 此时，这个语句是无效的，TypeError: d.resolve is not a function
</script>
</html>