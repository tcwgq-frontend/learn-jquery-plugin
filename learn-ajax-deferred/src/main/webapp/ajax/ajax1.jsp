<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- ${pageContext.request.contextPath } 这种方式直接取不出来值，怎么回事？-->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
</head>
<body>
	<h1>Deferred对象的使用</h1>
</body>
<script type="text/javascript">
	var wait = function() {
		var tasks = function() {
			alert('执行完毕');
		};
		setTimeout(tasks, 5000);
	};

	//wait();//5秒后执行完毕
	$.when(wait()).done(function() {
		alert('wait 执行完毕啦');
	}).fail(function() {
		alert('wait 执行失败了');
	});
	//马上执行，原因在于$.when()的参数是Deffered对象
	
	//改进如下
	var dtd = $.Deferred;
	var wait1 = function(dtd) {
		var tasks1 = function() {
			alert('执行完毕');
			dtd.resolve();//该表deferred对象的执行状态
		};
		setTimeout(tasks1, 5000);
		return dtd;
	};
	
	$.when(wait1(dtd)).done(function() {
		alert('wait1 执行完毕啦');
	}).fail(function() {
		alert('wait1 执行失败了');
	});
</script>
</html>